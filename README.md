# Jupyterhub_spawner_thread

## installation d'un serveur jupyterhub en mode threads avec authentification sous gitlab

## creer un token de groupe

 - **Scopes** 	

    api (Access the authenticated user's API)
    read_user (Read the authenticated user's personal information)
    read_api (Read Api)


 - **prendre le secret et l'id**
 ```python
  c.GitLabOAuthenticator.client_secret
  c.GitLabOAuthenticator.client_id 
```
![illustration](https://gitlab.paca.inrae.fr/lhoude/jupyterhub_spawner_thread/-/blob/main/creertokengroujupyter.png)

*éventuelement modifier le username sur gitlab nom.prenom ---->login_ldap pour que ça colle au ldap*




- **/etc/jupyterhub/jupyterhub_config.py**
```python
c.JupyterHub.ip = 'xxx.xxx.xxx.xxx'
c.JupyterHub.ssl_cert = '/etc/jupyterhub/jupyterhub.crt'
c.JupyterHub.ssl_key = '/etc/jupyterhub/jupyterhub.key'
from oauthenticator.gitlab import GitLabOAuthenticator
c.JupyterHub.authenticator_class = GitLabOAuthenticator
c.MyOAuthenticator.oauth_callback_url = 'https://ip_ou_nom_du_serveur/hub/oauth_callback'
c.JupyterHub.authenticator_class = 'oauthenticator.gitlab.LocalGitLabOAuthenticator'
c.Authenticator.delete_invalid_users = True
#c.LocalAuthenticator.create_system_users = True #la je l'ai enlevé ça avait l'air de craindre pour la secu

c.GitLabOAuthenticator.client_secret ='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
c.GitLabOAuthenticator.client_id = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
c.GitLabOAuthenticator.scope = ['read_user']
```

-----------------------------------------------------



**------------------ seules les lignes ci dessous suffisent pour creer l'utilisateur sur la machine:--------------------**
```python
from subprocess import check_call


def pre_spawn_hook(spawner):
    username = spawner.user.name
    try:
        check_call(['useradd', '-ms', '/bin/bash', username])
    except Exception as e:
        print(f'{e}')
c.pawner.pre_spawn_hook = pre_spawn_hook
```
# installation debian épurée spawner simple
``` bash
apt install python3-pip
sudo apt-get install npm
npm install -g npm
node -v
apt install libffi-dev
pip install --upgrade pip
python3 -m pip install jupyterhub
npm install -g configurable-http-proxy
python3 -m pip install notebook 
mkdir /srv/jupyterhub /etc/jupyterhub /var/log/jupyterhub

# on met le https
openssl genrsa -out jupyterhub.key 2048
openssl req -new -key jupyterhub.key -out jupyterhub.csr
openssl x509 -req -days 365 -in jupyterhub.csr -signkey jupyterhub.key -out jupyterhub.crt
echo "c.JupyterHub.ssl_cert = '/etc/jupyterhub/jupyterhub.crt'" >> jupyterhub_config.py
echo "c.JupyterHub.ssl_key = '/etc/jupyterhub/jupyterhub.key'" >> jupyterhub_config.py

#gitlab authentification
pip3 install oauthenticator

```

------------
Pour Mémoire ça peut reservir:
```python
#c.GitLabOAuthenticator.scope = ['read_user api read_repository write_repository']
async def add_auth_env(spawner):
  auth_state = await spawner.user.get_auth_state()

  if not auth_state:
    spawner.log.warning("No auth state for %s", spawner.user)
    return

  spawner.environment['GITLAB_ACCESS_TOKEN'] = auth_state['access_token']
  spawner.environment['GITLAB_USER_LOGIN'] = auth_state['gitlab_user']['username']
  spawner.environment['GITLAB_USER_ID'] = str(auth_state['gitlab_user']['id'])
  spawner.environment['GITLAB_USER_EMAIL'] = auth_state['gitlab_user']['email']
  spawner.environment['GITLAB_USER_NAME'] = auth_state['gitlab_user']['name']
  ```
-----------

*Le fichier à modifier pour choisir la forge d'authentification*

/usr/local/lib/python3.7/dist-packages/oauthenticator/gitlab.py

modifier les lignes de gitlab_url
```gitlab_url = Unicode("https://forgemia.inra.fr/", config=True)```
